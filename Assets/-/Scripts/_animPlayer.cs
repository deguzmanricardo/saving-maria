﻿using UnityEngine;
using System.Collections;

public class _animPlayer : MonoBehaviour
{
    public static _animPlayer This;
    public Animator _anim;
    public bool state = false;
    public string anim;

    void Awake() {
        This=this;
    }


    public void _automate()
    {
        StartCoroutine(_skillShow());
    }

    IEnumerator _skillShow()
    {
        _trigger();
        yield return new WaitForSeconds(2);
        if(state)
            _trigger();
    }

    public void _trigger()
    {
        _anim.Play(!state ? anim : "-"+anim);
        state = !state;
    }
}