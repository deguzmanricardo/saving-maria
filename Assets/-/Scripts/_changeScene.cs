﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class _changeScene : MonoBehaviour
{
    public static _changeScene This;

    public GameObject fader;
    public Animator _fadeAnim;
    string In = "In";

    string scene;

    void Awake()
    {
        This=this;
        Time.timeScale=0;
        fader.SetActive(true);
    }

    IEnumerator Start()
    {
         yield return new WaitForSecondsRealtime(1);
        // yield return new WaitForSeconds(1);
        fader.SetActive(false);
        Time.timeScale=1f;
    }

    public void _nextScene(string x)
    {
        scene = x;
        fader.SetActive(true);
        _fadeAnim.Play(In);
        StartCoroutine(_fader());
    }


    IEnumerator _fader()
    {
        yield return new WaitForSecondsRealtime(1);
        // yield return new WaitForSeconds(1);
        if(scene.Equals(""))
            Application.Quit();
        SceneManager.LoadScene(scene);
    }

    void Update()
    {
        if(Input.GetKey(KeyCode.LeftShift))
        {
            if(Input.GetKeyDown(KeyCode.KeypadMinus))
            {
                Time.timeScale = Time.timeScale == 0 ? 1 : 0;
            }
        }
    }
}