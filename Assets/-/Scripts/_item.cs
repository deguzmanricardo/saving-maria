﻿using UnityEngine;

public class _item : MonoBehaviour
{
    public GameObject _enable;

    void OnTriggerEnter(Collider other)
    {
        _enable.SetActive(false);
        gameObject.SetActive(false);
        _animPlayer.This._automate();
    }
}