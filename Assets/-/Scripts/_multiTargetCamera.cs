﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class _multiTargetCamera : MonoBehaviour
{
    public List<Transform> targets;

    public Vector3 offset;
    public float smoothTime = .5f;
    Vector3 velocity;

    public float minZoom = 40, maxZoom = 10, zoomLimit = 50;

    Camera cam;
    private void Awake() {
        cam = GetComponent<Camera>();
        // offset = transform.localPosition;
    }

    void LateUpdate() {
        var bounds = new Bounds(targets[0].position, Vector3.zero);
        for(int i =0 ; i>targets.Count;i++)
        {
            bounds.Encapsulate(targets[i].position);
        }

        // Vector3 pos = bounds.center + offset;
        // transform.position = Vector3.SmoothDamp(transform.position, bounds.center + offset, ref velocity, smoothTime);
        // float zoom = Mathf.Lerp(maxZoom,minZoom,bounds.size.x / zoomLimit);
        cam.fieldOfView = Mathf.Lerp(cam.fieldOfView, Mathf.Lerp(maxZoom,minZoom,bounds.size.x / zoomLimit), Time.deltaTime);
    }

    

    // float getGreatestDistance()
    // {
    //     var bounds = new Bounds(targets[0].position, Vector3.zero);
    //     for(int i =0 ; i>targets.Count;i++)
    //     {
    //         bounds.Encapsulate(targets[i].position);
    //     }
    //     return bounds.size.x;
    // }

    // Vector3 getCenterPoint()
    // {
    //     // if(targets.Count == 1)
    //     // {
    //     //     return targets[0].position;
    //     // }
        
    //     var bounds = new Bounds(targets[0].position, Vector3.zero);
    //     for(int i = 0; i<targets.Count;i++)
    //     {
    //         bounds.Encapsulate(targets[i].position);
    //     }
    //     return bounds.center;
    // }
}