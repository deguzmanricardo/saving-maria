﻿using UnityEngine;

public class _projectile : MonoBehaviour
{

    public float speed = 0.01f;
    Vector3 _default;
    Vector3 destination;
    public Transform objValidate;

#if UNITY_EDITOR
    void OnValidate() {
        // destination = objValidate ? objValidate.position : _battleManager.currentEnemy.body ? _battleManager.currentEnemy.body.position : null;
    }
#endif

    void OnTriggerEnter(Collider other)
    {
        _battleManager.currentEnemy._takeDamage();
        gameObject.SetActive(false);
    }

    void OnEnable()
    {
        _default = transform.position;
        destination =  _battleManager.currentEnemy.body.position;
    }

    void OnDisable()
    {
        transform.position = _default;
    }

    void Update()
    {
        transform.position = Vector3.Lerp(transform.position, destination, speed);
    }

}