﻿using UnityEngine;

public class _animTrigger : MonoBehaviour
{
    public void _playAudio(int x)
    {
        _trial.This.narration[x].Play();
    }

    public void _triggerScene(string x)
    {
        _changeScene.This._nextScene(x);
    }
}