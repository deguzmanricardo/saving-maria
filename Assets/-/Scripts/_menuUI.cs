﻿using UnityEngine;
using UnityEngine.UI;
public class _menuUI : MonoBehaviour
{
    public static _menuUI This;
    GameObject currentPanel;
    public Button[] chapter;
    int chapterX;
    public string[] levels;

    [Header("Audio")]
    public AudioSource btn;

    void Awake()
    {
        This = this;
        chapterX = chapter.Length;
    }

    //public int slotDebug, chara;
    //void Update()
    //{
    //    if (Input.GetKeyDown(KeyCode.Alpha9))
    //    {
    //        _profile.slot = slotDebug;
    //        PlayerPrefs.SetInt(slotDebug.ToString(), chara);
    //        Debug.Log(chara);
    //    }
    //}

    public void _changePanel(GameObject _obj)
    {
        btn.Play();
        if (currentPanel)
        {
            currentPanel.SetActive(false);
        }

        if (_obj.Equals(currentPanel))
        {
            currentPanel = null;
        }
        else
        {
            _obj.SetActive(true);
            currentPanel = _obj;
        }
    }

    public void _selectSlot(string x)
    {
        btn.Play();
        _profile.slot = int.Parse(x);
        if (PlayerPrefs.HasKey(x))
        {
            Debug.Log("GANA");
            _profile.chapter = PlayerPrefs.GetInt(x);
        }

        _chapterRefresh();
    }

    public void _chapterRefresh()
    {
        for (int x = 0; x < chapterX; x++)
        {
            chapter[x].interactable = _profile.chapter >= x ? enabled : false;
        }
    }

    public void _chapterSelect()
    {

    }
}