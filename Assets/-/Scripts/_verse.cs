﻿using UnityEngine;

[CreateAssetMenu(fileName = "verse", menuName = "Make Verse", order = 1)]
public class _verse : ScriptableObject
{
    public string title, verse;
    public bool isBible;
}