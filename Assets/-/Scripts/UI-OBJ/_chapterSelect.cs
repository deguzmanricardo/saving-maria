﻿using UnityEngine;
using UnityEngine.UI;

public class _chapterSelect : MonoBehaviour
{

    void Awake()
    {
        Button This;
        This = GetComponent<Button>();
        This.onClick.AddListener(() => _onClick());
    }

    void _onClick()
    {
        Text str = GetComponentInChildren<Text>();
        _changeScene.This._nextScene(_menuUI.This.levels[int.Parse(str.text.Substring(str.text.Length - 1, 1)) - 1]);
    }
}