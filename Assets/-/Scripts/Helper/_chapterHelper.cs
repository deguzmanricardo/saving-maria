﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEngine.UI;
using System.Text;

[ExecuteInEditMode]
public class _chapterHelper : MonoBehaviour
{
    public bool _update;
    int _len;

    void OnValidate()
    {
        OnEnable();
    }

    void OnEnable()
    {
        Text[] txt = GetComponentsInChildren<Text>();

        _len = txt.Length;

        StringBuilder sb = new StringBuilder();

        for (int x = 0; x < _len; x++)
        {
            sb.AppendFormat("Chapter {0}", x + 1); 
            txt[x].text = sb.ToString();
            sb.Clear();

            sb.AppendFormat("btn.{0}", x + 1);
            txt[x].transform.parent.name = sb.ToString();
            sb.Clear();
        }
        _update = false;
    }
}
#endif