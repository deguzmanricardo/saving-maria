using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class _enemy : MonoBehaviour
{
    public float life;
    public Text lifeText;
    Transform lifeRot;

    public Animation test;

    public const string DEATH	= "Anim_Death";

    public Transform body;
    public GameObject item;

    public GameObject cam;

    public UnityEvent onTrigger; 
    public bool oneTime, aggressive;
    bool init;


    void Awake() {
        this.enabled=false;
    }

    void Start() {
        lifeRot = lifeText.transform.parent;    
    }

    void Update() {
        lifeRot.LookAt(_player.cam.transform);
        lifeRot.Rotate(0,180,0);
    }

    void OnTriggerEnter(Collider other)
    {   
        if(other.tag.Equals("Player"))
        {
            _battleManager.currentEnemy = this;
            _gameUI.This._interact.interactable=true;
            _stateManager.This.trigger = _stateManager.state.battle;
            this.enabled=true;
        }

        if(!init)
        {
            init=true;
            onTrigger.Invoke();
        }
        
        if(aggressive)
        {
            _stateManager.This.battle.Invoke();
            onTrigger.Invoke();
        }
    }

    void OnTriggerExit(Collider other)
    {
        if(other.tag.Equals("Player"))
        {
            _gameUI.This._interact.interactable=false;
            _battleManager.currentEnemy = this ? null : _battleManager.currentEnemy;
            _gameUI.This._battleUI.SetActive(false);
            _stateManager.This.trigger = _stateManager.state.walk;
            this.enabled=false;
        }
    }

    private void OnTriggerStay(Collider other) {
        if(other.tag.Equals("Player"))
        {
            transform.LookAt(_player.This.transform);
        }
    }

    public void _takeDamage()
    {
        life--;
        lifeText.text=life.ToString();
        if(life==0)
        {
            _battleManager.currentEnemy=null;
            _gameUI.This._interact.interactable=false;
            DeathAni();
            Destroy(GetComponent<Collider>());
            item.SetActive(true);
            _stateManager.This.walk.Invoke();
            cam.SetActive(false);
            _player.cam.gameObject.SetActive(true);

            if(_stateManager.This.tutorialItem)
            {
                _stateManager.This.tutorialItem=false;
                _stateManager.This._tutItem.Invoke();
            }

        }
        else
        _gameUI.This._interact.interactable=true;
    }

	public void DeathAni (){
		test.CrossFade (DEATH);
    }
}