﻿using UnityEngine;
using UnityStandardAssets.Characters.ThirdPerson;
public class _thirdPersonInput : MonoBehaviour
{
    public FixedJoystick left;
    public FixedButton button;
    public FixedTouchField TouchField;

    protected ThirdPersonUserControl control;

    protected Camera cam;
    protected float CameraAngle;
    protected float CameraAngleSpeed = 0.2f;

    void Awake()
    {
        control = GetComponent<ThirdPersonUserControl>();
        cam = Camera.main;
    }

    void Update() {
        control.m_Jump = button.Pressed;
        control.hInput = left.Horizontal;
        control.vInput = left.Vertical;

        CameraAngle += TouchField.TouchDist.x * CameraAngleSpeed;

        cam.transform.position = transform.position + Quaternion.AngleAxis(CameraAngle, Vector3.up) * new Vector3(0, 3, 4);
        cam.transform.rotation = Quaternion.LookRotation(transform.position + Vector3.up * 2f - Camera.main.transform.position, Vector3.up);
    }

}