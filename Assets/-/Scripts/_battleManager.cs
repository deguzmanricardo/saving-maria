﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.Events;

public class _battleManager : MonoBehaviour
{

    public static _battleManager This;
    public static _enemy currentEnemy;

    public Text description;
    public GameObject orb;

    public bool tutorialBattle;
    public UnityEvent _tutBattle;

    void Awake() {
        This=this;
    }

    public float duration;

    public void _prebattle()
    {
        StartCoroutine(_preBattleCor());
    }

    IEnumerator _preBattleCor()
    {
        _player.cam.gameObject.SetActive(false);
        currentEnemy.cam.gameObject.SetActive(true);

        Vector3 enemyPos = new Vector3(currentEnemy.body.position.x, _player.This.transform.position.y,currentEnemy.body.position.z);

        _player.This.transform.LookAt(enemyPos);

        currentEnemy.transform.LookAt(_player.This.transform);
        
        // currentEnemy.cam.transform.eulerAngles = new Vector3(currentEnemy.cam.transform.eulerAngles.x,-100,currentEnemy.cam.transform.eulerAngles.z);

        yield return new WaitForSeconds(duration);
        _gameUI.This._battleUI.SetActive(true);
        _dbList.This._random();
    }

    public void _setVerse()
    {
        description.text = _dbList.This.current.title + "\n\n" + _dbList.This.current.verse;
    }

    public void _choices(bool bb)
    {
        if(tutorialBattle)
        {
            tutorialBattle=false;
            _tutBattle.Invoke();
        }
        _gameUI.This._battleUI.SetActive(false);
        if(bb.Equals(_dbList.This.current.isBible))
        {
            orb.SetActive(true);
        }
        else
        {
        }
    }

    public IEnumerator _correct()
    {
        orb.SetActive(false);
        currentEnemy._takeDamage();
        yield return null;
    }

    public IEnumerator _wrong()
    {
        _player.This._takeDamage();
        yield return null;
    }
}