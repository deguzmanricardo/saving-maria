﻿using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
using System;

[Serializable]
public struct mixer
{
    public String _name;
    public Slider slide;
}
public class _settings : MonoBehaviour
{
    public AudioMixer mixer;
    public string _name;

    public mixer[] control;

    void Start()
    {
        foreach(mixer con in control)
        {
            con.slide.value = PlayerPrefs.GetFloat(con._name,1);
            mixer.SetFloat(con._name,Mathf.Log10(con.slide.value) * 20);
            Debug.Log(con._name);
        }
    }

    public void _setParam(string tmp)
    {
        _name=tmp;
    }

    public void _setLevel(float slider)
    {
        mixer.SetFloat(_name,Mathf.Log10(slider) * 20);
        PlayerPrefs.SetFloat(_name,slider);
    }
}