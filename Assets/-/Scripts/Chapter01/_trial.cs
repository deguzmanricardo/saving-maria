﻿using UnityEngine;
using UnityEngine.UI;

public class _trial : MonoBehaviour
{
    public static _trial This;

    public int intAnim, intNarrate;

    public Button[] nav;
    public GameObject[] text;
    public AudioSource[] narration;
    public Animator animator;

    public string play;

    void Awake() {
        This=this;
    }

    public void _text(int x)
    {
        text[intNarrate].SetActive(false);
        narration[intNarrate].Stop();

        intNarrate=intNarrate+x;

        if(intNarrate < text.Length)
        {
            text[intNarrate].SetActive(true);
            narration[intNarrate].Play();
        }
        if(intNarrate==0)
        {
            nav[0].interactable=false;
        }
        else if(intNarrate >= text.Length)
        {
            nav[1].interactable=false;
            animator.Play(play);

        }
        else
        {
            nav[0].interactable = true;
            nav[1].interactable = true;
        }
    }

}