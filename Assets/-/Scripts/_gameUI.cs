﻿using UnityEngine;
using UnityEngine.UI;

public class _gameUI : MonoBehaviour
{
    public static _gameUI This;
    public GameObject _pauseUI;
    public GameObject _battleUI;
    public Button _interact;

    void Awake() {
        This=this;
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            _pause();
        }
    }

    public void _pause()
    {
            if(Time.timeScale==1)
            {
                Time.timeScale=0;
                _pauseUI.SetActive(true);
            }
            else
            {
                Time.timeScale=1;
                _pauseUI.SetActive(false);
            }
    }
}