﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class _delay : MonoBehaviour
{
    public float delay;
    public UnityEvent action;
    IEnumerator Start()
    {
        yield return new WaitForSeconds(delay);
        action.Invoke();
    }
}