﻿using UnityEngine;

public class _rotate : MonoBehaviour
{
    public Vector3 rot;
    public float spd;

    void Update()
    {
        transform.Rotate(rot,spd);
    }
}