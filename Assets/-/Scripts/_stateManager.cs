﻿using UnityEngine;
using UnityEngine.Events;


public class _stateManager : MonoBehaviour
{
    public static _stateManager This;

    public enum state{battle, item, walk};
    public UnityEvent battle, item, walk;

    public bool tutorialBattle = true, tutorialItem =true;
    public UnityEvent _tutBattle, _tutItem;

    public state trigger;

    void Awake() {
        This=this;
    }

    public void _trigger()
    {
        if(trigger.Equals(state.battle))
        {
            if(tutorialBattle)
            {
                tutorialBattle=false;
                _tutBattle.Invoke();
            }
            battle.Invoke();
        }
        else if(trigger.Equals(state.walk))
        {
            walk.Invoke();
        }
        else
        {
            item.Invoke();
        }
    }
}