﻿using UnityEngine;
using UnityEngine.UI;
public class _player : MonoBehaviour
{
    public static _player This;
    public static Camera cam;

    public float life;
    public Text lifeText;

    void Awake() {
        cam = Camera.main;
    }

    void Start() {
        This=this;
    }
    public void _takeDamage()
    {
        life--;
        lifeText.text=life.ToString();
    }

}